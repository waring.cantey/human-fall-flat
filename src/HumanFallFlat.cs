using Multiplayer;
using Steamworks;
using System;
using System.Linq;
using uMod.Common;
using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.HumanFallFlat
{
    /// <summary>
    /// The core Human: Fall Flat plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class HumanFallFlat : Plugin
    {
        #region Initialization

        internal static readonly HumanFallFlatProvider Universal = HumanFallFlatProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the HumanFallFlat class
        /// </summary>
        public HumanFallFlat()
        {
            // Set plugin info attributes
            Title = "Human: Fall Flat";
            Author = HumanFallFlatExtension.AssemblyAuthors;
            Version = HumanFallFlatExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            NetTransportSteam transport = NetGame.instance.transport as NetTransportSteam;
            if (transport != null)
            {
                // Make server public/open
                NetGame.friendly = HumanFallFlatExtension.FriendsOnly;
                Options.lobbyInviteOnly = HumanFallFlatExtension.InviteOnly ? 1 : 0;
                transport.SetJoinable(HumanFallFlatExtension.InviteOnly, true);
                transport.UpdateLobbyType();

                // Set/override max players
                Options.lobbyMaxPlayers = HumanFallFlatExtension.MaxPlayers;
                transport.UpdateLobbyPlayers();
                App.instance.OnClientCountChanged();

                // Allow join in progress
                Options.lobbyJoinInProgress = HumanFallFlatExtension.JoinInProgress ? 1 : 0;

                // Use cheat mode to enable/disable some stuff
                CheatCodes.cheatMode = true;

                // Override/set server hostname
                string serverName = $"{Server.Name} | {Server.Players}/{Server.MaxPlayers}";
                SteamMatchmaking.SetLobbyData(transport.lobbyID, "name", serverName);
            }

            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization

        #region Player Hooks

        /// <summary>
        /// Called when the player sends a message
        /// </summary>
        /// <param name="netHost"></param>
        /// <param name="playerName"></param>
        /// <param name="message"></param>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(NetHost netHost, string playerName, string message)
        {
            if (message.Trim().Length <= 1)
            {
                return true;
            }

            // TODO: Handle split screen players (same NetHost, different NetPlayer)

            // Select first local player
            NetPlayer netPlayer = netHost.players.FirstOrDefault();
            IPlayer player = netPlayer?.human?.IPlayer;
            if (player == null)
            {
                return null;
            }

            // Update player's stored username
            if (!player.Name.Equals(playerName))
            {
                player.Rename(playerName);
            }

            // Is the chat message a command?
            if (Universal.CommandSystem.HandleChatMessage(player, message) == CommandState.Completed)
            {
                return true;
            }

            // Call hook for plugins
            object chatUniversal = Interface.CallHook("OnPlayerChat", player, message);
            object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2022, 1, 1), player, message);
            object canChat = chatUniversal is null ? chatDeprecated : chatUniversal;

            // Is the chat message blocked?
            if (canChat != null)
            {
                return true;
            }

            // Log chat output
            Interface.uMod.LogInfo($"[Chat] {playerName}: {message}");
            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="human"></param>
        [Hook("IOnPlayerConnected")]
        private void IOnPlayerConnected(Human human)
        {
            // Check if server is intended to be dedicated
            if (human.player.isLocalPlayer && HumanFallFlatExtension.Dedicated)
            {
                // Remove server player from client list
                NetGame.instance.allclients.Remove(human.player.host);

                // Ignore server player
                return;
            }

            // TODO: Add command-line option/argument to allow/disallow split screen players
            /*if (human.player.host.players.Count > 1)
            {
                human.player.host.players.Remove(netPlayer);
                return;
            }*/

            // TODO: Kick duplicate players (already connected)

            Players.PlayerJoin(human.player.host.connection.ToString(), human.player.host.name); // human.player.nametag
            Players.PlayerConnected(human.player.host.connection.ToString(), human);

            IPlayer player = Players.FindPlayerById(human.player.host.connection.ToString());
            if (player != null)
            {
                // Set IPlayer object on Human
                human.IPlayer = player;

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2022, 1, 1), player);
            }

            // Override/set server hostname
            string serverName = $"{Server.Name} | {Server.Players}/{Server.MaxPlayers}";
            SteamMatchmaking.SetLobbyData((NetGame.instance.transport as NetTransportSteam).lobbyID, "name", serverName);
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="netHost"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(NetHost netHost)
        {
            // Select first local player
            NetPlayer netPlayer = netHost.players.FirstOrDefault();
            if (netPlayer == null)
            {
                return;
            }

            // Check if server is intended to be dedicated
            if (netPlayer.isLocalPlayer && HumanFallFlatExtension.Dedicated)
            {
                // Ignore server player
                return;
            }

            IPlayer player = netPlayer.human.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", player, "Unknown"); // TODO: Localization
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2022, 1, 1), player, "Unknown"); // TODO: Localization
            }

            Players.PlayerDisconnected(netPlayer.host.connection.ToString());

            // Override/set server hostname
            string serverName = $"{Server.Name} | {Server.Players}/{Server.MaxPlayers}";
            SteamMatchmaking.SetLobbyData((NetGame.instance.transport as NetTransportSteam).lobbyID, "name", serverName);
        }

        /// <summary>
        /// Called when the player respawns
        /// </summary>
        /// <param name="human"></param>
        [Hook("OnPlayerRespawn")]
        private void OnPlayerRespawn(Human human) => human.IPlayer.HasSpawnedBefore = true;

        #endregion Player Hooks
    }
}

using Multiplayer;
using System;
using System.Collections.Generic;
using System.Reflection;
using uMod.Common;
using uMod.Extensions;
using UnityEngine;

namespace uMod.Game.HumanFallFlat
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    [GameExtension]
    public class HumanFallFlatExtension : Extension
    {
        // Get assembly info
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);
        internal static string AssemblyAuthors = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly, typeof(AssemblyCompanyAttribute), false)).Company;

        // The command line
        public CommandLine CommandLine;

        // The configuration
        public static bool Dedicated { get; set; } = false;
        public static bool FriendsOnly { get; set; } = false;
        public static bool InviteOnly { get; set; } = false;
        public static bool JoinInProgress { get; set; } = true;
        public static int FpsLimit { get; set; } = 256;
        public static int MaxPlayers { get; set; } = 10;
        public static string ServerName { get; set; } = "My uMod Server";

        /// <summary>
        /// Gets whether this extension is for a specific game
        /// </summary>
        public override bool IsGameExtension => true;

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public override string Title => "Human: Fall Flat";

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => AssemblyAuthors;

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => AssemblyVersion;

        /// <summary>
        /// Gets the branch of this extension
        /// </summary>
        public override string Branch => "public"; // TODO: Handle this programmatically

        /// <summary>
        /// Commands that plugins are prevented from overriding
        /// </summary>
        internal static IEnumerable<string> RestrictedCommands => new[]
        {
            ""
        };

        /// <summary>
        /// Default game-specific references for use in plugins
        /// </summary>
        public override string[] DefaultReferences => new[]
        {
            "HumanAPI",
            "System.Drawing"
        };

        /// <summary>
        /// List of namespaces allowed for use in plugins
        /// </summary>
        public override string[] WhitelistedNamespaces => new[]
        {
            ""
        };

        /// <summary>
        /// Initializes a new instance of the HumanFallFlatExtension class
        /// </summary>
        public HumanFallFlatExtension()
        {
        }

        public override void HandleAddedToManager(IExtensionManager manager)
        {
            manager.RegisterPluginLoader(new HumanFallFlatPluginLoader(Interface.uMod.RootLogger));
        }

        /// <summary>
        /// Called when all other extensions have been loaded
        /// </summary>
        public override void OnModLoad()
        {
            // Parse command-line to set instance directory
            CommandLine = new CommandLine(Environment.GetCommandLineArgs());
            Dedicated = CommandLine.HasVariable("dedicated");
            FriendsOnly = CommandLine.HasVariable("friendsonly");
            InviteOnly = CommandLine.HasVariable("inviteonly");
            JoinInProgress = CommandLine.HasVariable("joininprogress");
            if (CommandLine.HasVariable("servername"))
            {
                CommandLine.GetArgument("servername", out _, out string serverName);
                ServerName = serverName;
            }

            // Check if server is intended to be dedicated
            if (Dedicated)
            {
                // Limit FPS to reduce CPU usage
                UnityEngine.Application.targetFrameRate = FpsLimit;

                // Disable game audio for server
                GameAudio.instance.SetMasterLevel(0f);

                // Forcefully host a game server
                App.state = AppSate.Menu;
                global::Game.multiplayerLobbyLevel = Options.multiplayerLobbyLevelStore;
                WorkshopRepository.instance.SetLobbyTitle(global::Game.multiplayerLobbyLevel);

                /*Options.multiplayerLobbyLevelStore = Game.multiplayerLobbyLevel;
                App.instance.StartGameServer(NetGame.instance.currentLevel, NetGame.instance.currentLevelType);
                NetGame.instance.transport.SetLobbyStatus(true);*/

                /*this.startedCheckpoint = 0;
                if (App.state == AppSate.ServerLobby)
                {
                    this.ExitLobby(true);
                    NetGame.instance.ServerLoadLevel(level, levelType, true, 0);
                    this.LaunchGame(level, levelType, 0, 0, () => this.LevelLoadedServer(level, levelType, Game.currentLevel.netHash));
                }*/

                NetGame.instance.transport.SetLobbyStatus(false);
                RatingMenu.instance.LoadInit();
                Dialogs.HideProgress();
                App.instance.HostGame();
                Physics.autoSimulation = true;
            }
        }
    }
}

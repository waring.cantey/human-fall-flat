using uMod.Auth;
using uMod.Common;

namespace uMod.Game.HumanFallFlat
{
    /// <summary>
    /// Represents a HumanFallFlat player manager
    /// </summary>
    public class HumanFallFlatPlayerManager : PlayerManager<HumanFallFlatPlayer>
    {
        /// <summary>
        /// Create a new instance of the HumanFallFlatPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public HumanFallFlatPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}

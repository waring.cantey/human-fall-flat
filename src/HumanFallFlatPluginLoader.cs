using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.HumanFallFlat
{
    /// <summary>
    /// Responsible for loading the core Human: Fall Flat plugin
    /// </summary>
    public class HumanFallFlatPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(HumanFallFlat) };

        public HumanFallFlatPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
